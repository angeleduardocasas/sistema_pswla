-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-11-2020 a las 00:34:47
-- Versión del servidor: 5.0.77
-- Versión de PHP: 5.3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_ausentismo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id_empleado` int(11) NOT NULL auto_increment,
  `apellido_empleado` varchar(124) collate utf8_spanish2_ci NOT NULL,
  `nombre_empleado` varchar(124) collate utf8_spanish2_ci NOT NULL,
  `legajo_empleado` varchar(32) collate utf8_spanish2_ci NOT NULL,
  `categoria_empleado` varchar(32) collate utf8_spanish2_ci NOT NULL,
  `dni_empleado` varchar(32) collate utf8_spanish2_ci NOT NULL,
  `situacion_revista` varchar(32) collate utf8_spanish2_ci NOT NULL,
  `activo_empleado` int(1) NOT NULL,
  PRIMARY KEY  (`id_empleado`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id_empleado`, `apellido_empleado`, `nombre_empleado`, `legajo_empleado`, `categoria_empleado`, `dni_empleado`, `situacion_revista`, `activo_empleado`) VALUES
(1, 'Casasola', 'Angel Eduard', '2453', 'Administrativo', '28187297', 'Permanente', 1),
(2, 'pedraza', 'luis alberto', '2345', 'Servicio', '28187296', 'Contratado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id_log` int(11) NOT NULL auto_increment,
  `log` int(11) NOT NULL,
  `fechahora` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY  (`id_log`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `logs`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL auto_increment,
  `usuario` varchar(64) collate utf8_spanish2_ci NOT NULL,
  `clave` varchar(64) collate utf8_spanish2_ci NOT NULL,
  `apelido_usuario` varchar(64) collate utf8_spanish2_ci NOT NULL,
  `nombre_usuario` varchar(64) collate utf8_spanish2_ci NOT NULL,
  `activo` int(1) NOT NULL,
  PRIMARY KEY  (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `clave`, `apelido_usuario`, `nombre_usuario`, `activo`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'Admin', 1);
