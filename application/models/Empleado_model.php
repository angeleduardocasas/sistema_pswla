<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Empleado_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function find($buscado='') {
        $resultado = array();
        $sql = "SELECT * 
                FROM empleados AS e "
                . "WHERE e.activo_empleado=1";
        if ($buscado != '') {
            if (is_integer($buscado)) {
                if ($buscado < 20000)
                    $sql.=" AND e.legajo_empleado='" . $buscado . "'";
                else
                    $sql.=" AND e.legajo_dni='" . $buscado . "'";
            } else {
                $buscado = str_replace(",", '', $buscado);
                $sql.=" AND CONCAT(e.apellido_empleado,'',e.nombre_empleado) LIKE '%" . $buscado . "%'";
            }
        }
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function deleteEmpleado($idempleado=0) {
        $this->db->update('empleados', array('activo_empleado' => 0), array('id_empleado' => $idempleado));
        return true;
    }

    public function saveEmpleado($apellido, $nombre, $dni, $legajo, $categoria, $revista) {
        $this->db->insert('empleados', array('apellido_empleado' => $apellido, 'nombre_empleado' => $nombre, 'dni_empleado' => $dni, 'legajo_empleado' => $legajo, 'categoria_empleado' => $categoria, 'situacion_revista' => $revista, 'activo_empleado' => 1));
        return $this->db->insert_id();
    }

    public function getEmpleado($idempleado=0) {
        if ($idempleado == 0 || !isset($idempleado))
            return false;
        $objeto = $this->db->query("SELECT * FROM empleados WHERE id_empleado=" . (int) $idempleado);
        if ($objeto)
            return $objeto->row();
        return false;
    }

    public function updateEmpleado($idempleado, $apellido, $nombre, $dni, $legajo, $categoria, $revista) {
        $resumen = sha1($apellido);
        $this->db->update('empleados', array('apellido_empleado' => $apellido, 'nombre_empleado' => $nombre, 'dni_empleado' => $dni, 'legajo_empleado' => $legajo, 'categoria_empleado' => $categoria, 'situacion_revista' => $revista, 'codigo_verificacion' => $resumen), array('id_empleado' => $idempleado));
        return true;
    }

    public function findPartesBy($idempleado=0) {
        $resultados= array();
        if ($idempleado == 0 || !isset($idempleado))
            return $resultados;
        $objeto = $this->db->query("SELECT * FROM partes WHERE id_empleado=" . (int) $idempleado);
        if ($objeto)
            $resultados=$objeto->result();
        return $resultados;
    }

}