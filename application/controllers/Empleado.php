<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Empleado extends CI_Controller {

    public
    function __Construct() {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('empleado_model');
    }

    public function index() {
        if (!file_exists('application/views/login.php'))
            show_error('Lo sentimos la pagina no esta disponible en estos momentos - Comuniquese con el administrador de Sistema');
        die('-----');
    }

    public function listado() {
        $buscado = '';
        if ($this->input->post('buscado'))
            $buscado = trim($this->input->post('buscado'));
        $list_empleados = $this->empleado_model->find($buscado);
        $vector_variable_formulario = array('variable_busqueda' => $buscado, 'action' => 'empleado/listado');
        $form_busqueda = $this->load->view("formulario_busqueda", $vector_variable_formulario, TRUE);
        $vector_variable_vista = array('formulario_busqueda' => $form_busqueda, 'empleados' => $list_empleados);
        $contenido = $this->load->view("empleado/listado", $vector_variable_vista, true);
        $this->load->view("principal", array('contenido' => $contenido));
    }

    public function eliminarEmpleado() {
        $idempleado = (int) $this->input->post('idempleado');
        if (!is_numeric($idempleado) || !isset($idempleado))
            die('recurso inexistente');
        $this->empleado_model->deleteEmpleado($idempleado);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino el empleado</div>');
        redirect('empleado/listado');
    }

    public function nuevoEmpleado() {
        $contenido = $this->load->view("empleado/nuevo", array(), TRUE);
        $this->load->view("principal", array('contenido' => $contenido));
    }

    public function guardarEmpleado() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('dni', 'DNI', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Empleado porque no completo los datos obligatorios.</div>');
            redirect('empleado/nuevoEmpleado');
        } else {
            $apellido = trim($this->input->post('apellido'));
            $nombre = $this->input->post('nombre');
            $dni = $this->input->post('dni');
            $legajo = $this->input->post('legajo');
            $categoria = trim($this->input->post('categoria'));
            $revista = trim($this->input->post('revista'));
            $idempleado = $this->empleado_model->saveEmpleado($apellido, $nombre, $dni, $legajo, $categoria, $revista);
            if ($idempleado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo.</div>');
            }
            redirect('empleado/listado');
        }
    }

    public function editEmpleado($idempleado=0) {
        $empleado = $this->empleado_model->getEmpleado($idempleado);
        if (!$empleado) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Recurso inexistente.</div>');
            redirect('empleado/listado');
        }
        $contenido = $this->load->view("empleado/edit", array('empleado' => $empleado), TRUE);
        $this->load->view("principal", array('contenido' => $contenido));
    }
    
     public function actualizarEmpleado() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('dni', 'DNI', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Empleado porque no completo los datos obligatorios.</div>');
            redirect('empleado/editEmpleado/'.(int)$this->input->post('id_empleado'));
        } else {
            $apellido = trim($this->input->post('apellido'));
            $nombre = $this->input->post('nombre');
            $dni = $this->input->post('dni');
            $legajo = $this->input->post('legajo');
            $categoria = trim($this->input->post('categoria'));
            $revista = trim($this->input->post('revista'));
            $idempleado=$this->input->post('id_empleado');
            $exito = $this->empleado_model->updateEmpleado($idempleado,$apellido, $nombre, $dni, $legajo, $categoria, $revista);
            if ($exito) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('empleado/listado');
        }
    }
    
    public function verDetalle($idempleado=0)
    {
     $empleado = $this->empleado_model->getEmpleado($idempleado);
     $contenido = $this->load->view("empleado/detalle", array('empleado'=>$empleado), TRUE);
     $this->load->view("principal", array('contenido' => $contenido));    
    }
    
    public function findPartesBy()
    {
      $idempleado=(int)$this->input->post('idempleado'); 
      $partes=$this->empleado_model->findPartesBy($idempleado); 
      $html='<table>';
      foreach ($partes as $parte)
        $html.="<tr><td>".$parte->fecha_parte."</td><td>".$parte->motivo_parte."</td></tr>";
      $html.="</table>";
      echo json_encode($html);
    }

}
