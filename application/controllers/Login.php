<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller {

    public
    function __Construct() {
        parent::__Construct();
        $this->load->model('login_model');
    }
    
    public function index()
    {
     if (!file_exists('application/views/login.php'))
            show_error('Lo sentimos la pagina no esta disponible en estos momentos - Comuniquese con el administrador de Sistema');
        $this->load->view('login');
    }
    
    public function validar() {        
        $data = array();
        $this->form_validation->set_rules('usuario', 'Usuario', 'required|trim');
        $this->form_validation->set_rules('clave', 'Contraseña', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No ingreso los campos obligatorios</div>');
            redirect('login/index', 'refresh');
        } else {
            $usuario = $this->login_model->autenticar_login(trim($this->input->post('usuario')), trim($this->input->post('clave')));
            if ($usuario) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Inicio sesion correctamente</div>');
                $data = array ('id_usuario' => $usuario->id_usuario,'usuario'	 =>$usuario->usuario,'logged_in' => TRUE);
		$this -> session -> set_userdata($data);
                redirect('empleado/listado', 'refresh');
            } else {               
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Usuario/Clave incorrectos</div>');
                redirect('login', 'refresh');
            }
        }
    }
    
    public
    function logout() {        
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
