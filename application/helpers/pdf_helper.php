<?php
if (!defined('BASEPATH'))
    exit('No tiene acceso al directorio');
require_once '/tcpdf/tcpdf.php';

/**
 * @class Pdf
 * @brief
 * @version 1.0
 * @author Casasola Angel Eduardo
 */
class Pdf extends TCPDF {

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false);
    }

}

?>
