<script type="text/javascript">
    $(document).ready(function () {
        $('#tabla_recursos').DataTable({"paging": false});
    });
    function setEliminar(id) {
        $("#idempleado").attr('value', id);        
    }
</script>
<?= $formulario_busqueda ?>
<div><?= $this->session->flashdata('mensaje'); ?> </div>
<h1 class="text-center">Empleados <a data-toggle="tooltip" title="Nuevo Empleado" href="<?= site_url('empleado/nuevoEmpleado') ?>"><i class="fas fa-user-plus"></i></a></h1>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table id="tabla_recursos" class="table">
                <thead>
                    <tr>
                        <th >Legajo</th>
                        <th class="center-align">Apellido</th>
                        <th class="center-align">Nombre</th>
                        <th class="center-align">DNI</th>
                        <th class="center-align">Categor&iacute;a</th>
                        <th class="center-align">Revista</th>                        
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($empleados as $empleado): ?>
                        <tr id="tr-<?= $empleado->id_empleado ?>">
                            <td><?= $empleado->legajo_empleado ?></td>
                            <td><?= $empleado->apellido_empleado ?></td>
                            <td><?= $empleado->nombre_empleado ?></td>
                            <td><?= $empleado->dni_empleado ?></td>
                            <td><?= $empleado->categoria_empleado ?></td>
                            <td><?= $empleado->situacion_revista ?></td>
                            <td class="center-align">
                                <?= anchor('impresion/imprimirLegajo/'.$empleado->id_empleado, '<i class="fas fa-file-pdf"></i>')?>
                                <a data-toggle="tooltip" title="Editar" href="<?= site_url('empleado/editEmpleado/' . $empleado->id_empleado) ?>"><i class="fa fa-edit"></i></a>
                                <a href="javascript:void()" onclick="setEliminar('<?= $empleado->id_empleado ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                <a href="<?= site_url('empleado/verDetalle/' . $empleado->id_empleado) ?>" title="Ver Detalle"><i class="fas fa-search-plus"></i></a>                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Modal de Eliminar -->
<div class="modal" id="myModalEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Cabezera -->
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Empleado</h4>                
            </div>
            <!-- Modal Cuerpo -->
            <?= form_open('empleado/eliminarEmpleado', 'id="formulario_eliminar""') ?>
            <div class="modal-body">
                <input type="hidden" value="" name="idempleado" id="idempleado"/> 
                Esta seguro de Eliminar el Empleado?
            </div>
            <!-- Modal Pie -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
<!-- Fin de Modal de Fecha -->