<script type="text/javascript">
    $(document).ready(function () {
        $("#hrefver").click(function(){
            $.ajax(
            {type: 'POST',
                url: "<?= site_url('empleado/findPartesBy') ?>",
                data: {'idempleado': <?= $empleado->id_empleado ?>},
                dataType: 'JSON',
                success: function (dato)
                {                   
                    $("#divajax").html(dato);
                }              
            });
        });
    });
</script>

<h2>Datos del Empleado</h2>
<div class="row">
    <div class="col-1"><label for="">Apellido</label></div>
    <div class="col-5"><?= $empleado->apellido_empleado ?></div>
    <div class="col-1"><label for="nombre" >Nombre</label></div>
    <div class="col-5"><?= $empleado->nombre_empleado ?></div>
</div>
<div class="row">
    <div class="col-1"><label for="dni">DNI</label></div>
    <div class="col-5"><?= $empleado->dni_empleado ?></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>
<h2>Datos de Laborales</h2>
<div class="row">
    <div class="col-1"> <label for="Legajo">Legajo</label></div>
    <div class="col-5"><?= $empleado->legajo_empleado ?></div>
    <div class="col-1">  <label>Categor&iacute;a</label></div>
    <div class="col-5"><?= $empleado->categoria_empleado ?></div>
</div>

<div class="row">
    <div class="col-1"><label for="revista">Situaci&oacute;n Revista</label></div>
    <div class="col-5"><?= $empleado->situacion_revista ?></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>
<div class="row">
    <div class="col-1"><a href="javascript:void(0)" id="hrefver" >Ver partes</a></div>
    <div class="col-11"></div>
</div>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table id="tabla_recursos" class="table">
                <thead><tr><th>Fecha</th><th>Motivo</th></tr></thead>
                <tbody>

                </tbody>        
            </table>
        </div>
    </div>
</div>