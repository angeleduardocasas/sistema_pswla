<div><?= $this->session->flashdata('mensaje'); ?> </div>
<h1 class="text-center bg-light">Nuevo Empleado</h1>
<?= form_open('empleado/guardarEmpleado', 'class="was-validated" id="frm_empleado"') ?>
<h2>Datos del Empleado</h2>
<div class="row form-group">
    <div class="col-1"><input type="hidden" name="id_empleado" id="id_empleado"/><label for="">Apellido</label></div>
    <div class="col-5"><input type="text" id="apellido" name="apellido" class="form-control"/></div>
    <div class="col-1"><label for="nombre" >Nombre</label></div>
    <div class="col-5"><input type="tel" id="nombre" name="nombre" class="form-control"/></div>
</div>
<div class="row form-group">
    <div class="col-1"><label for="dni">DNI</label></div>
    <div class="col-5"><input id="dni" name="dni" type="text" class="form-control"/></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>
<h2>Datos de Laborales</h2>
<div class="row form-group">
    <div class="col-1"> <label for="Legajo">Legajo</label></div>
    <div class="col-5"><input id="legajo" class="form-control" type="number" name="legajo"/></div>
    <div class="col-1">  <label>Categor&iacute;a</label></div>
    <div class="col-5"> <?= form_dropdown("categoria", array('Administrativo' => 'Administrativo', 'Servicio' => 'Servicio'), '0', array('id' => "categoria", 'class' => 'form-control')) ?></div>
</div>

<div class="row form-group">
    <div class="col-1"><label for="revista">Situaci&oacute;n Revista</label></div>
    <div class="col-5"><?= form_dropdown("revista", array('Permanente' => 'Permanente', 'Contratado' => 'Contratado'), array(), array('id' => "revista", 'class' => 'form-control')) ?></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>

<div class="row form-group">
    <div class="col-4"></div>
    <div class="col-4"><button type="bootton" class="btn btn-danger">Cancelar</button>  </div>        
    <div class="col-4"><button type="submit" class="btn btn-primary">Guardar</button>  </div>
</div>
<?= form_close() ?>
<script type="text/javascript">
    $("document").ready(function () {         
        $('#frm_empleado').submit(function(){ 
            $("#submit_guardar").attr('disabled', true);
        });       
    });
</script>