<div class="container">
<h2>Datos del Empleado</h2>
<div>
    <div class="col-1"><label for="">Apellido</label></div>
    <div class="col-5"><?=$empleado->apellido_empleado?></div>
    <div class="col-1"><label for="nombre" >Nombre</label></div>
    <div class="col-5"><?=$empleado->nombre_empleado?></div>
</div>
<div>
    <div class="col-1"><label for="dni">DNI</label></div>
    <div class="col-5"><?=$empleado->dni_empleado?></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>
<h2>Datos de Laborales</h2>
<div>
    <div class="col-1"> <label for="Legajo">Legajo</label></div>
    <div class="col-5"><?=$empleado->legajo_empleado?></div>
    <div class="col-1">  <label>Categor&iacute;a</label></div>
    <div class="col-5"><?=$empleado->categoria_empleado?></div>
</div>

<div>
    <div class="col-1"><label for="revista">Situaci&oacute;n Revista</label></div>
    <div class="col-5"><?= $empleado->situacion_revista?></div>
    <div class="col-1"></div>
    <div class="col-5"></div>
</div>
<div>