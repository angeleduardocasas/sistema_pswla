<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">             
        <title>Sistema de Ausentismo</title>		
        <script src='<?= base_url() ?>assets/js/jquery-3.1.1.js'></script>
        <link href="<?= base_url() ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>	
        <link href="<?= base_url() ?>assets/css/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#usuario").focus();
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div><?= $this->session->flashdata('mensaje'); ?> </div>
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="card p-4">
                        <?= form_open('login/validar') ?>
                        <div class="card-header text-center text-uppercase h4 font-weight-light">
                            Ingreso al Sistema
                        </div>

                        <div class="card-body py-5">
                            <label class="form-control-label">Usuario</label>
                            <div class="form-group">
                                <input type="text" class="form-control" name="usuario" id="usuario" required="required"/>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Contrase&ntilde;a</label>
                                <input type="password" class="form-control" name="clave" id="clave" required="required"/>
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-8">
                                    <button  type="submit" class="btn btn-primary px-5">Iniciar sesi&oacute;n</button>
                                </div>

                            </div>
                        </div>
                        <?= form_close() ?>                       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
