<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset='utf-8' />		
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Sistema de Ausentismo</title>		
        <script src='<?= base_url() ?>assets/js/moment.min.js'></script>
        <script src='<?= base_url() ?>assets/js/jquery-3.1.1.js'></script>
        <script src='<?= base_url() ?>assets/js/jquery-ui.js'></script>
        <script src='<?= base_url() ?>assets/js/fullcalendar.js'></script>		
        <script src='<?= base_url() ?>assets/js/locale-all.js'></script>
        <script src='<?= base_url() ?>assets/js/jquery.maskedinput.js'></script>
        <script src='<?= base_url() ?>assets/js/bootstrap.js'></script>
        <script src='<?= base_url() ?>assets/js/jquery.dataTables.js'></script>                 
        <link href="<?= base_url() ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/fullcalendar.print.css" rel="stylesheet" media="print" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>	
        <link href="<?= base_url() ?>assets/css/jquery.dataTable.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/css/all.css" rel="stylesheet" type="text/css"/>	
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-md navbar-light btn-info">
                        <a class="navbar-brand"><img src=""/></a>
                        <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="nav">
                            <ul class="navbar-nav">
                                <li class="nav-item" ><a class="nav-link text-light font-weight-bold px-3" href="<?= site_url('empleado/listado') ?>">Inicio</a></li>											
                                <li class="nav-item"><a class="nav-link text-light font-weight-bold px-3" href="<?= site_url('login/logout') ?>">Cerrar sesi&oacute;n</a></li>
                                <li class="nav-item"><a class="nav-link text-light font-weight-bold px-3" href="javascript:void(0)">Usuario: <?= $this->session->userdata('usuario') ?></a></li>
                            </ul>				
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-12">                    
                    <?= $contenido ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-md navbar-light btn-info text-ligh font-weight-bold px-3">&copy; <?= Date('Y') ?> Copyright  Dianix - Todos los derechos reservados - angeleduardocasas@gmail.com</nav>			
                </div>
            </div>
        </div>
    </body>
</html>